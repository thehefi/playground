// C++ program for implementation
// of Bubble sort
#include <bits/stdc++.h>
#include <chrono>

using namespace std;

size_t fillType = 0;
const unsigned int itemCount = 100;
std::vector<int> m_array(itemCount);

void FillData(unsigned int schema)
{
	// printf("FillData(%d): %d\n",schema,itemCount);
    if (schema == 0) // Shuffle of [1,n]
    {
        for (size_t i = 0; i < itemCount; i++)
            m_array[i] = i;

        std::random_shuffle(m_array.begin(), m_array.end());
    }
    else if (schema == 1) // Ascending [1,n]
    {
        for (size_t i = 0; i < m_array.size(); ++i)
		{
			// printf("%d:",i);
			m_array[i] = i;
			// printf("%d ",m_array[i]);
		}
		// printf("\n");
}
    else if (schema == 2) // Descending [1,n]
    {
        for (size_t i = 0; i < itemCount; i++)
            m_array[i] = itemCount - i;
    }
    else if (schema == 3) // Cubic skew of [1,n]
    {
        for (size_t i = 0; i < itemCount; i++)
        {
            // normalize to [-1,+1]
            double x = (2.0 * (double)i / itemCount) - 1.0;
            // calculate x^3
            double v = x * x * x;
            // normalize to array size
            double w = (v + 1.0) / 2.0 * itemCount + 1;
            // decrease resolution for more equal values
            w /= 3.0;
            m_array[i] = w + 1;
        }

        std::random_shuffle(m_array.begin(), m_array.end());
    }
    else if (schema == 4) // Quintic skew of [1,n]
    {
        for (size_t i = 0; i < itemCount; i++)
        {
            // normalize to [-1,+1]
            double x = (2.0 * (double)i / itemCount) - 1.0;
            // calculate x^5
            double v = x * x * x * x * x;
            // normalize to array size
            double w = (v + 1.0) / 2.0 * itemCount + 1;
            // decrease resolution for more equal values
            w /= 3.0;
            m_array[i] = w + 1;
        }

        std::random_shuffle(m_array.begin(), m_array.end());
    }
    else if (schema == 5) // shuffled n-2 equal values in [1,n]
    {
        m_array[0] = 1;
        for (size_t i = 1; i < itemCount-1; i++)
        {
            m_array[i] =  itemCount / 2 + 1 ;
        }
        m_array[itemCount-1] = itemCount;

        std::random_shuffle(m_array.begin(), m_array.end());
    }
    else // fallback
    {
        return FillData(0);
    }

}

// A function to implement bubble sort
void bubbleSort()
{
	int i, j;
	for (i = 0; i < itemCount - 1; i++)

		// Last i elements are already
		// in place
		for (j = 0; j < itemCount - i - 1; j++)
			if (m_array[j] > m_array[j + 1])
				swap(m_array[j], m_array[j + 1]);
}

void showArray()
{
	int i;
	for (i = 0; i < itemCount; i++)
	{
		cout << m_array[i] << " ";
	}
	cout << endl;
}

// Driver code
int main()
{
	FillData(fillType);

	printf("Unsorted array: \n");
	showArray();

	auto start = chrono::steady_clock::now();

    bubbleSort();

	auto end = chrono::steady_clock::now();

	printf("Sorted array: \n");
	showArray();

    // double clocks_taken = chrono::duration_cast<chrono::nanoseconds>(end - start).count();
	std::cout << chrono::duration_cast<chrono::nanoseconds>(end - start).count() << " nanoseconds.";
	std::cout << std::endl;

	return 0;
}
// This code is contributed by rathbhupendra
