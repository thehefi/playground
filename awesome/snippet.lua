-- Move client to workspace on another monitor
awful.key({ modkey, "Shift" }, "m",
    function ()
            local screen = awful.screen.focused()
                    local tag = screen.tags[2] -- change this to the tag you want to move the client to
                            local client = client.focus
                                    if client then
                                                client:move_to_tag(tag)
                                                            awful.screen.focus(1) -- change this to the screen you want to move the client to
                                                                        client:tags({tag})
                                                                                    client:raise()
                                                                                                client:emit_signal("request::activate")
                                                                                                            client:raise()
                                                                                                                    end
                                                                                                                        end,
                                                                                                                            {description = "Move client to workspace on another monitor", group = "client"})
                                                                                                                            