My Table
========

Overview
--------

Sample
~~~~~~

.. list-table::
   :widths: auto
   :header-rows: 1

   * - Zeitraum
     - Auftraggeber                         
     - Projekt
   * - 2007-2008
     - Statistik Austria                    
     - Datawarehouse
   * - 2003
     - gnc (Global Network Communication)
     - Content Management System
   * - 2003
     - GNC Akademie
     - ORACLE Training
   * - 2003
     - gnc – Global Network Communication
     - ORACLE Consulting
   * - 2002
     - BLSG - Business Logisic Systems GmbH
     - Konzept zur Unternehmensweiterführung
   * - 1999-2001
     - Siemens Business Services, München
     - CISÖ (Cargo Information System Österreich)
   * - 1999
     - BLSG (Business Logisic Systems GmbH)
     - Netzwerkinfrastruktur
