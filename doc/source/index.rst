.. image:: _images/playground.png
   :alt: PlayGround
   :align: right

======================
thehefi PlayGround doc
======================

Check out the :doc:`appendix/rdt` section for information about how this documentation interacts whith ReadTheDocs and Sphinx.

.. note:: This project is under development.

.. toctree::
   :maxdepth: 1
   :caption: Subprojects
   :hidden:

   subprojects

.. toctree::
   :maxdepth: 1
   :caption: Read the Docs Examples
   :hidden:

   rdt/tables
   rdt/mytable

.. toctree::
   :maxdepth: 1
   :caption: Appendix
   :hidden:

   appendix/rdt
